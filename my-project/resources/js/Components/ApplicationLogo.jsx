import logo from "../assets/favicon/favicon.ico"

export default function ApplicationLogo(props) {
    return (
        <img src={logo} alt="Logo" />
    );
}
