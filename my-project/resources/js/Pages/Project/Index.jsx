import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";

import { render } from "react-dom";
import DashboardTaskList from "@/Layouts/DashboardTaskList";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

export default function Dashboard({ auth }) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                    Dashboard
                </h2>
            }
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 dark:text-gray-100">

                            <div>
                                <span className="m-[20px]">
                                    Select Project
                                </span>
                                <select name="" id="" className="border-black rounded">
                                    <optgroup label="Current Proejcts">
                                    <option value="">Project 1</option>
                                    <option value="">Project 2</option>
                                    <option value="">Project 3</option>
                                    <option value="">Project 4</option>
                                    </optgroup>
                                    <optgroup label="New">
                                    <option value="">+ Create a New Project</option>
                                    </optgroup>
                                </select>

                            </div>

                            <div className="flex flex-row justify-between py-5">
                                <input
                                    placeholder="Task Name"
                                    name="taskName"
                                    id="taskName"
                                    className="border-[1px] border-black rounded p-2 w-5/12"
                                />
                                <input
                                    placeholder="Priority"
                                    name="priority"
                                    id="priority"
                                    className="border-[1px] border-black rounded p-2 w-1/12"
                                />
                                <input
                                    name="taskDatetime"
                                    id="taskDatetime"
                                    type="datetime-local"
                                    className="border-[1px] border-black rounded p-2 w-3/12"
                                />

                                <button
                                    class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                                        text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                                        focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                                >
                                    Create Task
                                </button>
                            </div>

                            <div className="w-auto border-[1px] border-black flex p-3 bg-[rgba(79,70,229,0.4)]">
                                <div className="w-6/12 font-bold">Task Name</div>
                                <div className="w-1/12 font-bold">Priority</div>
                                <div className="w-3/12 font-bold">Task Date time</div>
                                <div className="w-2/12 font-bold">Actions</div>
                            </div>
                            <DndProvider backend={HTML5Backend}>
                                <DashboardTaskList />
                            </DndProvider>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
