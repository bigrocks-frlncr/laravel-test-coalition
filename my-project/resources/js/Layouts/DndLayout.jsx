import update from "immutability-helper";
import { useCallback, useState } from "react";
import { Card } from "@/Components/Card.jsx";

export const Container = () => {
    {
        const newLocal = <div>asdfasdfasdfasd</div>;
        const [cards, setCards] = useState([
            {
                id: 1,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get Klaviyo revunue</div>
                        <div className="w-1/12">1</div>
                        <div className="w-3/12">12/21/2023 14:30</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 2,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get G Ads Dat</div>
                        <div className="w-1/12">2</div>
                        <div className="w-3/12">12/22/2023 09:00</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 3,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Check error log of FB</div>
                        <div className="w-1/12">3</div>
                        <div className="w-3/12">12/25/2023 10:00</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 4,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunueGet Klaviyo revunue</div>
                        <div className="w-1/12">1</div>
                        <div className="w-3/12">12/21/2023 14:30</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 5,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get Klaviyo revunue</div>
                        <div className="w-1/12">1</div>
                        <div className="w-3/12">12/21/2023 14:30</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 6,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get Klaviyo revunue</div>
                        <div className="w-1/12">1</div>
                        <div className="w-3/12">12/21/2023 14:30</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
            {
                id: 7,
                text: (
                    <div className="flex">
                        <div className="w-6/12 truncate pr-[10px]">Get Klaviyo revunue</div>
                        <div className="w-1/12">1</div>
                        <div className="w-3/12">12/21/2023 14:30</div>
                        <div className="w-2/12 cursor-pointer flex justify-around">
                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Edit
                            </button>

                            <button
                                class="flex justify-center rounded-md bg-indigo-600 p-2 px-8 text-sm font-semibold leading-6
                            text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2
                            focus-visible:outline-offset-2 focus-visible:outline-indigo-600 w-2/12"
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                ),
            },
        ]);
        const moveCard = useCallback((dragIndex, hoverIndex) => {
            setCards((prevCards) =>
                update(prevCards, {
                    $splice: [
                        [dragIndex, 1],
                        [hoverIndex, 0, prevCards[dragIndex]],
                    ],
                })
            );
        }, []);
        const renderCard = useCallback((card, index) => {
            return (
                <Card
                    key={card.id}
                    index={index}
                    id={card.id}
                    text={card.text}
                    moveCard={moveCard}
                />
            );
        }, []);
        return (
            <>
                <div className="w-auto">
                    {cards.map((card, i) => renderCard(card, i))}
                </div>
            </>
        );
    }
};
